# Interface Design

# Dance Studio Website

This repository contains the code for the Dance Studio website. The website provides information about the dance studio, including the schedule of classes and contact information.

## Goals

The main goals of this project are:

- **Information Provision**: The application is designed to serve as a comprehensive source of information about the dance studio, detailing the types of dance classes available, the schedule, and the location.
- **Student Engagement**: The application aims to engage potential students by showcasing the dance studio's expertise, team, and facilities.
- **Ease of Contact**: The application facilitates easy contact with the dance studio via phone or email for appointments or inquiries.
- **User Experience Enhancement**: The application is designed to offer a pleasant user experience through a well-designed interface and visually appealing features such as sliders.

# Basic Interaction Scenario

- **Homepage Visit**: Upon landing on the homepage, the user is presented with an appealing header section that showcases the dance studio's expertise. The navigation bar allows easy access to different sections like About, Schedule, and Contacts.
- **About Section Exploration**: By clicking on the "About" link in the navigation bar, the user is presented with information about the dance studio's ethos, staff, and commitment to excellence. The text is accompanied by engaging visuals.
- **Schedule Viewing**: The user can navigate to the "Schedule" section to view the timetable of dance classes.
- **Contact Information Retrieval**: If the user wishes to find contact details, they can scroll down or click on the "Contacts" link in the navigation bar. The Contact section displays the phone number, email address, and a map showing the dance studio's location. The user can click on the phone number or email address to initiate contact.

# Advanced Interaction Scenario

- **Dance Class Booking**: If the user wishes to book a dance class, they can view the "Schedule" section, decide on a class, and then click on the "Contacts" link to find contact information. The user can then call the provided phone number to schedule a dance class.
- **Dance Studio Location**: If the user wishes to visit the dance studio but is unfamiliar with the area, they can navigate to the "Contacts" section. The interactive map provides clear directions to the dance studio's location. The user can zoom in/out and switch between map views for better understanding.
- **Special Offers Inquiry**: If the user is interested in any ongoing promotions or special offers, they can explore the footer section and find links to social media or promotions page. The user can click on the link and discover current offers, discounts, or events happening at the dance studio.

Link to Mockup (Figma):
https://www.figma.com/file/JLVWNm6TcH7vXGvHw0b1tp/Untitled?type=design&node-id=9-27&mode=design&t=U5z0DSYb9WsHoxaH-0