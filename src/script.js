let navbar = document.querySelector('.header .navbar');

document.querySelector('#menu-btn').onclick = () => {
    navbar.classList.toggle('active');
}

window.onscroll = () => {
    navbar.classList.remove('active');
}

var swiper = new Swiper(".home-slider", {
    loop: true,
    grabCursor: true,
    speed: 900,
    autoplay: {
        delay: 3700, // время задержки между слайдами (в миллисекундах)
        disableOnInteraction: false, // продолжать или нет автопроигрывание после взаимодействия пользователя
    },
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
});

var swiper = new Swiper(".clasess-slider", {
  loop: true,
  grabCursor: true,
  spaceBetween: 20,
  brekpoints: {
    640: {
      slidesPerView:1,
    },
    768: {
      slidesPerView:2,
    },
    991: {
      slidesPerView:3,
    },
  }
});